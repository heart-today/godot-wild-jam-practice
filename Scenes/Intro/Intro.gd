extends Node

onready var description = $Description
onready var audio = $AudioStreamPlayer

func _unhandled_key_input(key : InputEventKey):
	if key.pressed:
		if key.scancode == KEY_ESCAPE:
			if audio.playing:
				audio.stop()
			else:
				audio.play()
				
		else:
			description.visible = not description.visible
	
