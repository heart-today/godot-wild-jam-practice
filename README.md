
# Godot Wild Jam Practice

Intended for content creators to practice using [Git](https://git-scm.com/) and [Gitlab](https://gitlab.com) to add content to a Godot project

## Demo

HTML5 export demo of this repo hosted on [Their Temple](http://theirtemple.com/gwjp/gwjp.html)

## Example

These are the exact steps for your first piece of content added.

In this example, we are going to add a profile picture for Hanumanji.

This means:
- Adding the resource under the Authors/ directory
- (Optional) Creating a Scene for the new content
- (Optional) Showing the Scene in the game play

Why are these last two optional?  You are welcome to do them, but I am also offering to help with that step if you don't know how.


- [Download Git](https://git-scm.com/downloads) or [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Clone this repo
    ```
    > git clone https://gitlab.com/heart-today/godot-wild-jam-practice
    ```
- Enter the directory into which you cloned the repo, and take a look around

    ```
    > cd godot-wild-jam-practice
    > tree
    .
    ├── Authors
    │   └── Hanumanji
    │       ├── intro.ogg
    │       └── intro.ogg.import
    ├── README.md
    ├── Scenes
    │   └── Intro
    │       ├── Intro.gd
    │       └── Intro.tscn
    ├── default_env.tres
    ├── export_presets.cfg
    ├── icon.png
    ├── icon.png.import
    └── project.godot
    ```     
- Check the status
    ```
    > git status
    On branch master
    Your branch is up to date with 'origin/master'.

    nothing to commit, working tree clean
    ```
    This tells you that you are on the master branch.  If you make any changes here, you won't be able to push them back to the remote repo on Gitlab.
- Create your branch for the content you are adding
    ```
    > git checkout -b hanumanji-add-profile
    Switched to a new branch 'hanumanji-add-profile'
    ```
- Run the game for the first time 

    (Launch Godot & from Project Manager Import the project.godot file then click Play button in upper right)

    This is important because it lets you know the state of the game BEFORE you make any changes to it.
    You can also compare it to the [web demo](http://theirtemple.com/gwjp/gwjp.html)
- Create your content directory under the Authors/ directory (if it doesn't already exist)
    ```
    > mkdir Authors/Hanumanji
    ```
- Add your content into your author directory

    `> mv ~/profile.png Authors/Hanumanji`

- (Optional)Create a Godot scene for your content

   In this case, I made a Scene called Hanumanji, to represent me in this world we are all building.
   It's simply a Node with a Sprite to contain the profile.png resource
   Then I went ahead and made a script to move the HanumanjiScene around.
    
- Run the game again, to check your changes
- (Optional) Close Godot

    This is good because it forces you to evaluate any open files with changes still pending.

- 
